const express = require('express')
const router = express.Router()
const clients_usersController = require('../controllers/clients_users.controller')
router.get('/', clients_usersController.findAll)
router.post('/', clients_usersController.create)
router.get('/:id', clients_usersController.findById)
router.put('/:id', clients_usersController.update)
router.delete('/:id', clients_usersController.delete)
module.exports = router