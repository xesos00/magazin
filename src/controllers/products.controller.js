'use strict'
const Products = require('../models/products.model')
exports.findAll = function(req, res) {
    Products.findAll(function(err, products){
        console.log("controller")
        if (err) 
        res.send(err)
        console.log('res', products)
        res.send(products)
    })
}
exports.create = function(req,res){
    const new_products = new Products(req.body)
    if ( req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({error:true, message: "Please provide all required fields"})
    }else{
        Products.create(new_products, function(err, products) {
            if(err)
            res.send(err)
            res.json({error:false, message:"product Added", data: products})
        })
    }
}
exports.findById = function(req,res) {
    Products.findById(req.params.id, function(err, products) { 
         if (err)  res.send(err);  res.json(products)
        })
}
exports.update = function(req,res){
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){ 
        res.status(400).send({ error:true, message: 'Please provide all required field' })}
    else{
        Products.update(req.params.id, new products(req.body), function(err, products){
            if(err)
            res.send(err)
            res.json({error:false, message:"product updated"})
        })
    }
}
exports.delete = function(req,res) {
    Products.delete(req.params.id, function(err, products) {
        if(err)
        res.send(err)
        res.json({error:false, message:"product deleted"})
    })
}