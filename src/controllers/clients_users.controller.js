'use strict'
const Clients_users = require('../models/client_users.model')
exports.findAll = function(req, res) {
    Clients_users.findAll(function(err, clients_users){
        console.log("controller")
        if (err) 
        res.send(err)
        console.log('res', clients_users)
        res.send(clients_users)
    })
}
exports.create = function(req,res){
    const new_clients_users = new Clients_users(req.body)
    if ( req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({error:true, message: "Please provide all required fields"})
    }else{
        Clients_users.create(new_clients_users, function(err, clients_users) {
            if(err)
            res.send(err)
            res.json({error:false, message:"Client Added", data: clients_users})
        })
    }
}
exports.findById = function(req,res) {
    Clients_users.findById(req.params.id, function(err, clients_users) { 
         if (err)  res.send(err);  res.json(clients_users)
        })
}
exports.update = function(req,res){
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){ 
        res.status(400).send({ error:true, message: 'Please provide all required field' })}
    else{
        Clients_users.update(req.params.id, new Clients_users(req.body), function(err, clients_users){
            if(err)
            res.send(err)
            res.json({error:false, message:"Client updated"})
        })
    }
}
exports.delete = function(req,res) {
    Clients_users.delete(req.params.id, function(err, clients_users) {
        if(err)
        res.send(err)
        res.json({error:false, message:"Client deleted"})
    })
}