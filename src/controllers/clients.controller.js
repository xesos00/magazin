"use strict";
const Clients = require("../models/clients.model");
exports.findAll = function (req, res) {
  Clients.findAll(function (err, clients) {
    console.log("controller");
    if (err) res.send(err);
    console.log("res", clients);
    res.send(clients);
  });
};
exports.create = function (req, res) {
  const new_clients = new Clients(req.body);
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
      res.end()
  } else {
    Clients.create(new_clients, function (err, clients) {
      if (err) res.send(err);
      res.json({ error: false, message: "Client Added", data: clients });
      res.end()
    });
  }
};
exports.findById = function (req, res) {
  Clients.findById(req.params.id, function (err, clients) {
    if (err) res.send(err);
    res.json(clients);
  });
};
exports.update = function (req, res) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Clients.update(req.params.id, new Clients(req.body), function (
      err,
      clients
    ) {
      if (err) res.send(err);
      res.json({ error: false, message: "Client updated" });
    });
  }
};
exports.delete = function (req, res) {
  Clients.delete(req.params.id, function (err, clients) {
    if (err) res.send(err);
    res.json({ error: false, message: "Client deleted" });
  });
};
