'use strict'
var dbConnection = require('./../../config/db.config')
const e = require('express')
var Client_Users = function(client_users) { 
    this.id = client_users.id
    this.telegram_id = client_users.telegram_id
    this.client_id = client_users.client_id
    
}
Client_Users.create = function(newClient_user, result){
    dbConnection.query("INSERT INTO client_users set ?", newClient_user, function(err, res) {
        if( err ){
            console.log("error: ", err)
            result(err, null)
        }
        else{
            console.log(res.insertId)
            result (null, res.insertId)
        }
    })
}
Client_Users.findById = function ( id, result) {
    dbConn.query("Select * from client_users where id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err)  
            result(null, err)
        }
        else{  
        console.log('employees : ', res)
        result(null, res);
    }
    })
}
Client_Users.findAll = function(result){
    dbConnection.query("SELECT * FROM client_users", function(err, res) {
        if (err) {
            console.log("error: ", err)
            result(null, err)
        } else {
            console.log("client_users: ", res)
            result(null, res)
        }
    })
}
Client_Users.update = function(id, client_users, result) {
    dbConnection.query("UPDATE client_users SET telegram_id=?, client_id=? WHERE id = ?", [client_users.telegram_id, client_users.client_id, id], function(err, res) {
        if (err) {
            console.log("error:", err)
            result(null, err)
        }
        else{
            result(null, res)
        }
    })
}
Client_Users.delete = function(id, result){
    dbConnection.query("DELETE FROM client_users WHERE id= ? ", [id], function(err, res){
        if (err) {
            console.log("Error: ", err)
            result(null, err)
        } else {
            result(null, res)
        }
    })
}
module.exports = Client_Users