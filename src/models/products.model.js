'use strict'
var dbConnection = require('./../../config/db.config')
const e = require('express')
var Products = function(products) { 
    this.id = products.id
    this.client_id = products.client_id
    this.title =  products.title
    this.description = products.description
    this.price = products.price
    this.oldPrice = products.oldPrice
    this.create_at = new Date()
    this.update_at = new Date()
}
Products.create = function(newClient, result){
    dbConnection.query("INSERT INTO products set ?", newClient, function(err, res) {
        if( err ){
            console.log("error: ", err)
            result(err, null)
        }
        else{
            console.log(res.insertId)
            result (null, res.insertId)
        }
    })
}
Products.findById = function ( id, result) {
    dbConn.query("Select * from products where id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err)  
            result(null, err)
        }
        else{  
        console.log('employees : ', res)
        result(null, res);
    }
    })
}
Products.findAll = function(result){
    dbConnection.query("SELECT * FROM products", function(err, res) {
        if (err) {
            console.log("error: ", err)
            result(null, err)
        } else {
            console.log("products: ", res)
            result(null, res)
        }
    })
}
Products.update = function(id, products, result) {
    dbConnection.query("UPDATE products SET client_id=?, title=?, description=?, price=?, oldPrice=? WHERE id = ?", [products.client_id, products.title, products.description, products.price, products.oldPrice, id], function(err, res) {
        if (err) {
            console.log("error:", err)
            result(null, err)
        }
        else{
            result(null, res)
        }
    })
}
Products.delete = function(id, result){
    dbConnection.query("DELETE FROM products WHERE id= ? ", [id], function(err, res){
        if (err) {
            console.log("Error: ", err)
            result(null, err)
        } else {
            result(null, res)
        }
    })
}
module.exports = Products