"use strict";
var dbConnection = require("./../../config/db.config");
var Clients = function (clients) {
  this.id = clients.id;
  this.token = clients.token;
  this.name = clients.name;
  this.info = clients.info;
  this.address = clients.address;
  this.status = clients.status ? clients.status : 1;
  this.create_at = new Date();
  this.update_at = new Date();
};
Clients.create = function (newClient, result) {
  dbConnection.query("INSERT INTO clients set ?", newClient, function (
    err,
    res
  ) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res.insertId);
      result(null, res.insertId);
    }
  });
};














Clients.findById = function (id, result) {
  dbConn.query("Select * from clients where id = ? ", id, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      console.log("employees : ", res);
      result(null, res);
    }
  });
};
Clients.findAll = function (result) {
  dbConnection.query("SELECT * FROM clients", function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      console.log("clients: ", res);
      result(null, res);
    }
  });
};
Clients.update = function (id, clients, result) {
  dbConnection.query(
    "UPDATE clients SET name=?, info=?, address=?, status=? WHERE id = ?",
    [clients.name, clients.info, clients.address, clients.status, id],
    function (err, res) {
      if (err) {
        console.log("error:", err);
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};
Clients.delete = function (id, result) {
  dbConnection.query("DELETE FROM clients WHERE id= ? ", [id], function (
    err,
    res
  ) {
    if (err) {
      console.log("Error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};
module.exports = Clients;
