const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 5000
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


const clientsRoutes = require('./src/routes/clients.routes')
app.use('/api/v1/clients', clientsRoutes)

// const clients_usersRoutes = require('./src/routes/clients_users.routes')
// app.use('/api/v1/clients_users', clients_usersRoutes)

// const productsRoutes = require('./src/routes/products.routes')
// app.use('/api/v1/products', productsRoutes)



app.listen(port, () => {
    console.log(`Server is listening on ${port}`)
})
